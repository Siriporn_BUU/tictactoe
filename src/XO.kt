import java.util.*

fun main(args: Array<String>) {
    val kb = Scanner(System.`in`)
    val rand = Random()
    val table = Table()
    var player = -1
    var comp = -1
    println(table.tab)
    while(true) {
        println("Which cell number do you want to mark? ")
        player = kb.nextInt() - 1
        while (table.cells[player].state != CellState.EMPTY) {
            print("Cell ${player + 1} has already been chosen. Please pick a new cell ")
            player = kb.nextInt() - 1
        }
        table.cells[player].state = CellState.CROSS
        if (table.isFull() || !(table.winState() == CellState.EMPTY)) break
        comp = player
        while ((table.cells[comp].state != CellState.EMPTY)) comp = rand.nextInt(9)
        table.cells[comp].state = CellState.CIRCLE
        if (table.isFull() || !(table.winState() == CellState.EMPTY)) break
        println(table)
    }
    println(table)
    when(table.winState()) {
        CellState.EMPTY -> println("This game was a draw")
        CellState.CIRCLE -> println("You have lost the game")
        CellState.CROSS -> println("You have won the game")
    }
}
private enum class CellState(val string: String, val value: Int) {
    EMPTY(" ", 0), CIRCLE("O", 1), CROSS("X", 2)
}
private data class Cell(var state: CellState = CellState.EMPTY){
    fun equals(other: Cell): Boolean = this.state == other.state
}
private class Table {
    val cells = arrayOf(Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell(), Cell())
    override fun toString(): String =
            " " + cells[0].state.string + " | " + cells[1].state.string + " | " + cells[2].state.string + "\n" +
                    "-----------" + "\n" +
                    " " + cells[3].state.string + " | " + cells[4].state.string + " | " + cells[5].state.string + "\n" +
                    "-----------" + "\n" +
                    " " + cells[6].state.string + " | " + cells[7].state.string + " | " + cells[8].state.string + "\n"
    val tab =
            "Board XO position" + "\n" +
            " " + "1" + " | " + "2" + " | " + "3" + "\n" +
            "----------" + "\n" +
            " " + "4" + " | " + "5" + " | " + "6" + "\n" +
            "----------" + "\n" +
            " " + "7" + " | " + "8" + " | " + "9" + "\n"
    fun winState() : CellState {
        if((cells[0].equals(cells[1]) && cells[0].equals(cells[2])) || (cells[0].equals(cells[3]) && cells[0].equals(cells[6])) || (cells[0].equals(cells[4]) && cells[0].equals(cells[8]))) return cells[0].state
        if(cells[1].equals(cells[4]) && cells[1].equals(cells[7])) return cells[1].state
        if((cells[2].equals(cells[4]) && cells[2].equals(cells[6])) || (cells[2].equals(cells[5]) && cells[2].equals(cells[8]))) return cells[2].state
        if(cells[6].equals(cells[7]) && cells[6].equals(cells[8])) return cells[6].state
        if(cells[3].equals(cells[4]) && cells[3].equals(cells[5])) return cells[3].state
        return CellState.EMPTY
    }
    fun isFull(): Boolean {
        for(cell in cells) if(cell.state.value==0) return false
        return true
    }
}